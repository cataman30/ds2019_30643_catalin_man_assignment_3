package com.example.springdemo.assignment3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.springdemo.protos.MedicationPlanService;

import java.io.IOException;

@Configuration
public class GrpcServer  {

    private MedicationPlanService medicalPlanService;

    @Autowired
    public GrpcServer(MedicationPlanService medicalPlanService){
        this.medicalPlanService=medicalPlanService;
    }

    @Bean
     public void startGrpc() throws IOException, InterruptedException {

        Thread grpcThread= new Thread(new GrpcThread(medicalPlanService));
        grpcThread.start();

    }

}