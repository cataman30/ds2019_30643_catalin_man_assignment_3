package com.example.springdemo.assignment3;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

import com.example.springdemo.protos.MedicationPlanService;

public class GrpcThread implements Runnable {

    Server server;
    public GrpcThread(MedicationPlanService medicalPlanService) {
        this.server =ServerBuilder.forPort(9090).addService(medicalPlanService).build();
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
            server.start();
            System.out.println("SERVER STARTED#############################################################################################################################################################");
            server.awaitTermination();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}