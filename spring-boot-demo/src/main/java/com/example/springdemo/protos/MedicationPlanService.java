package com.example.springdemo.protos;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.engine.query.spi.sql.NativeSQLQueryCollectionReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springdemo.entities.Medicationintake;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.protos.MedicationPlan.MedicationIntakeRequest;
import com.example.springdemo.protos.MedicationPlan.MedicationTakenMessage;
import com.example.springdemo.protos.MedicationPlan.MedicationTakenRequest;
import com.example.springdemo.protos.MedicationPlan.ResponseMessage;
import com.example.springdemo.protos.medicationIntakeGrpc.medicationIntakeImplBase;
import com.example.springdemo.repositories.MedicationRepository;
import com.example.springdemo.repositories.MedicationintakeRepository;
import com.example.springdemo.services.MedicationService;
import com.example.springdemo.services.MedicationintakeService;

import io.grpc.stub.StreamObserver;

@Service
public class MedicationPlanService extends medicationIntakeImplBase{
	
	@Autowired
	MedicationintakeRepository medicationIntakeRepository;
	@Autowired
	MedicationRepository medicationRepository;
	
	public String removeLastChar(String s) {
	    return (s == null || s.length() == 0)
	      ? null
	      : (s.substring(0, s.length() - 1));
	}
	
	@Override
	public void getMedicationIntake(MedicationIntakeRequest request, StreamObserver<ResponseMessage> responseObserver) {

		int patientID = 1;//request.getPatientId();
		MedicationintakeService miService = new MedicationintakeService(medicationIntakeRepository);
		MedicationService medService = new MedicationService(medicationRepository);
		ResponseMessage.Builder response = ResponseMessage.newBuilder(); 
		
		List<Medicationintake> intakes = miService.findAll();
		List<Medicationintake> neededIntakes = new ArrayList<Medicationintake>();
		
		for (Medicationintake m : intakes) {
			if(m.getPatient().getId() == patientID)
				neededIntakes.add(m);
		}
		
		String firstHour = new String();
		String secondHour = new String();
		String start = new String();
		String end = new String();
		String medicationName = new String();
		
		for(Medicationintake m : neededIntakes)
		{
			String[] tokens = m.getIntakeInterval().split("-");
			firstHour = firstHour + "#" + tokens[0];
			secondHour = secondHour + "#" + tokens[1];
			start = start + "#" + m.getStart();
			end = end + "#" + m.getEnd();
			medicationName = medicationName + "#" + medService.findMedicationById(m.getMedication().getId()).getName();
		}
		
		removeLastChar(firstHour);
		removeLastChar(secondHour);
		removeLastChar(start);
		removeLastChar(end);
		removeLastChar(medicationName);
		firstHour = firstHour.substring(1);
		secondHour = secondHour.substring(1);
		start = start.substring(1);
		end = end.substring(1);
		medicationName = medicationName.substring(1);
		
		response.setFirstHour(firstHour);
		response.setSecongHour(secondHour);
		response.setEnd(end);
		response.setStart(start);
		response.setMedicationName(medicationName);
		
		responseObserver.onNext(response.build());
		responseObserver.onCompleted();
		
	}
	
	public void notifyMedicationTaken(MedicationTakenRequest request, StreamObserver<MedicationTakenMessage> responseObserver) {
		MedicationTakenMessage.Builder response = MedicationTakenMessage.newBuilder(); 
		System.out.println(request.getMessage());
		responseObserver.onNext(response.build());
		responseObserver.onCompleted();
	}
}
