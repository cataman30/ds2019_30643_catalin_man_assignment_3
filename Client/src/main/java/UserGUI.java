import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import io.grpc.netty.shaded.io.netty.util.Timer;
import protos.MyTimer;
import protos.MedicationPlan.ResponseMessage;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class UserGUI {

	private JFrame frame;
	private JTable table;
	private JLabel timerLabel = new JLabel("");
	private String timeToDownload = "00:00:00";
	JButton takeMedicationButton = new JButton("Take medication");
	GrpcClient client = new GrpcClient();
	JLabel dateLabel = new JLabel("");
	String date;
	Boolean buttonFlag;

	public MyTimer mytimer = new MyTimer();


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserGUI window = new UserGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public UserGUI() {
		initialize();
		mytimer.start();
		
		new Thread(() -> {
			try {
				while (true) {
					Thread.sleep(1000);
					String time = mytimer.getTime();
					date = mytimer.getCurrentDate();
					
					timerLabel.setText(time);
					dateLabel.setText(date);
					
					if(time.contentEquals(timeToDownload))
					{
						ResponseMessage rMessage = client.RequestMedicationPlan();

						DefaultTableModel model = (DefaultTableModel) table.getModel();

						String[] firstHours = rMessage.getFirstHour().split("#");
						String[] secondHours = rMessage.getSecongHour().split("#");
						String[] starts = rMessage.getStart().split("#");
						String[] ends = rMessage.getEnd().split("#");
						String[] medicationNames = rMessage.getMedicationName().split("#");

						int numberOfElements = firstHours.length;

						for (int i = 0; i < numberOfElements; i++)
							if(isWithinRange(date, starts[i], ends[i]))
							model.addRow(new Object[] { starts[i], ends[i], firstHours[i], secondHours[i], medicationNames[i]});
					}
					buttonFlag = false;
					takeMedicationButton.setVisible(buttonFlag);
					DefaultTableModel dm=(DefaultTableModel)table.getModel();
					for(int row = 0; row < dm.getRowCount(); row++) 
						{
							if(mytimer.checkHour((String)table.getValueAt(row, 2),(String)table.getValueAt(row, 3)))
								buttonFlag=true;
							if(mytimer.checkPassed((String)dm.getValueAt(row, 3)))
							{
								client.notifyServerNoTakenMessage((String)dm.getValueAt(row, 4));
								((DefaultTableModel)table.getModel()).removeRow(row);
							}
						}
					takeMedicationButton.setVisible(buttonFlag);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}).start();
	}

	
	boolean isWithinRange(String testDate, String startDate, String endDate){
		   
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date td = null, sd = null, ed = null;
		try {
			td = sdf.parse(testDate);
			sd = sdf.parse(startDate);
			ed = sdf.parse(endDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return !(td.before(sd) || td.after(ed));
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 799, 499);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 94, 734, 285);
		frame.getContentPane().add(scrollPane);

		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "From(Date)", "To(Date)", "From(Hour)", "From(Hour)", "Name of Medication"}));

		scrollPane.setViewportView(table);

		JButton displayMedicationPlan = new JButton("Display medication plan");
		displayMedicationPlan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ResponseMessage rMessage = client.RequestMedicationPlan();

				DefaultTableModel model = (DefaultTableModel) table.getModel();

				String[] firstHours = rMessage.getFirstHour().split("#");
				String[] secondHours = rMessage.getSecongHour().split("#");
				String[] starts = rMessage.getStart().split("#");
				String[] ends = rMessage.getEnd().split("#");
				String[] medicationNames = rMessage.getMedicationName().split("#");

				int numberOfElements = firstHours.length;

				for (int i = 0; i < numberOfElements; i++)
				{
					if(isWithinRange(date, starts[i], ends[i]))
					model.addRow(new Object[] { starts[i], ends[i], firstHours[i], secondHours[i], medicationNames[i]});
				}
			}
		});

		timerLabel.setBounds(632, 34, 122, 33);
		frame.getContentPane().add(timerLabel);

		displayMedicationPlan.setBounds(20, 34, 207, 33);
		frame.getContentPane().add(displayMedicationPlan);
		
		dateLabel.setBounds(482, 34, 122, 33);
		frame.getContentPane().add(dateLabel);
		
		JLabel lblSelectTheMedication = new JLabel("Select the medication and press the button:");
		lblSelectTheMedication.setBounds(10, 387, 594, 14);
		frame.getContentPane().add(lblSelectTheMedication);
		
		
		takeMedicationButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int rowNo = table.getSelectedRow();
				if(mytimer.checkHour((String)table.getValueAt(rowNo, 2),(String)table.getValueAt(rowNo, 3)))
					{
						((DefaultTableModel)table.getModel()).removeRow(rowNo);
						 client.NotifyServer();
					}
				else {
					System.out.println("not in interval");
				}
			}
		});
		takeMedicationButton.setBounds(10, 412, 328, 23);
		frame.getContentPane().add(takeMedicationButton);
	}
}
