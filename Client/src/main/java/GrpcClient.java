import protos.MedicationPlan.ResponseMessage;

import org.omg.CORBA.Request;

import com.google.protobuf.Empty;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import protos.MedicationPlan.MedicationIntakeRequest;
import protos.MedicationPlan.MedicationTakenMessage;
import protos.MedicationPlan.MedicationTakenRequest;
import protos.medicationIntakeGrpc;
import protos.medicationIntakeGrpc.medicationIntakeBlockingStub;

public class GrpcClient {

	public ResponseMessage RequestMedicationPlan()
	{
		ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9090).usePlaintext().build();
		
		medicationIntakeBlockingStub medicationIntakeStub = medicationIntakeGrpc.newBlockingStub(channel);
		
		MedicationIntakeRequest medicationIntakeRequest = MedicationIntakeRequest.newBuilder().setPatientId("1").build();
		
		ResponseMessage responseMessage = medicationIntakeStub.getMedicationIntake(medicationIntakeRequest);
		
		return responseMessage;
	}
	
	public MedicationTakenMessage NotifyServer()
	{
		ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9090).usePlaintext().build();
		medicationIntakeBlockingStub medicationIntakeStub = medicationIntakeGrpc.newBlockingStub(channel);
		
		MedicationTakenRequest medicationTakenRequest = MedicationTakenRequest.newBuilder().setMessage("The patient took his medication in the required interval!").build();
		MedicationTakenMessage medicationTakenMessage = medicationIntakeStub.notifyMedicationTaken(medicationTakenRequest);
		return medicationTakenMessage;
	}
	
	public MedicationTakenMessage notifyServerNoTakenMessage(String medication)
	{
		ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9090).usePlaintext().build();
		medicationIntakeBlockingStub medicationIntakeStub = medicationIntakeGrpc.newBlockingStub(channel);
		
		MedicationTakenRequest medicationTakenRequest = MedicationTakenRequest.newBuilder().setMessage("The patient didn't take the "+medication+"medication!").build();
		MedicationTakenMessage medicationTakenMessage = medicationIntakeStub.notifyMedicationTaken(medicationTakenRequest);
		return medicationTakenMessage;
	}
}
