package protos;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: medicationPlan.proto")
public final class medicationIntakeGrpc {

  private medicationIntakeGrpc() {}

  public static final String SERVICE_NAME = "medicationIntake";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<protos.MedicationPlan.MedicationIntakeRequest,
      protos.MedicationPlan.ResponseMessage> getGetMedicationIntakeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getMedicationIntake",
      requestType = protos.MedicationPlan.MedicationIntakeRequest.class,
      responseType = protos.MedicationPlan.ResponseMessage.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<protos.MedicationPlan.MedicationIntakeRequest,
      protos.MedicationPlan.ResponseMessage> getGetMedicationIntakeMethod() {
    io.grpc.MethodDescriptor<protos.MedicationPlan.MedicationIntakeRequest, protos.MedicationPlan.ResponseMessage> getGetMedicationIntakeMethod;
    if ((getGetMedicationIntakeMethod = medicationIntakeGrpc.getGetMedicationIntakeMethod) == null) {
      synchronized (medicationIntakeGrpc.class) {
        if ((getGetMedicationIntakeMethod = medicationIntakeGrpc.getGetMedicationIntakeMethod) == null) {
          medicationIntakeGrpc.getGetMedicationIntakeMethod = getGetMedicationIntakeMethod = 
              io.grpc.MethodDescriptor.<protos.MedicationPlan.MedicationIntakeRequest, protos.MedicationPlan.ResponseMessage>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "medicationIntake", "getMedicationIntake"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protos.MedicationPlan.MedicationIntakeRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protos.MedicationPlan.ResponseMessage.getDefaultInstance()))
                  .setSchemaDescriptor(new medicationIntakeMethodDescriptorSupplier("getMedicationIntake"))
                  .build();
          }
        }
     }
     return getGetMedicationIntakeMethod;
  }

  private static volatile io.grpc.MethodDescriptor<protos.MedicationPlan.MedicationTakenRequest,
      protos.MedicationPlan.MedicationTakenMessage> getNotifyMedicationTakenMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "notifyMedicationTaken",
      requestType = protos.MedicationPlan.MedicationTakenRequest.class,
      responseType = protos.MedicationPlan.MedicationTakenMessage.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<protos.MedicationPlan.MedicationTakenRequest,
      protos.MedicationPlan.MedicationTakenMessage> getNotifyMedicationTakenMethod() {
    io.grpc.MethodDescriptor<protos.MedicationPlan.MedicationTakenRequest, protos.MedicationPlan.MedicationTakenMessage> getNotifyMedicationTakenMethod;
    if ((getNotifyMedicationTakenMethod = medicationIntakeGrpc.getNotifyMedicationTakenMethod) == null) {
      synchronized (medicationIntakeGrpc.class) {
        if ((getNotifyMedicationTakenMethod = medicationIntakeGrpc.getNotifyMedicationTakenMethod) == null) {
          medicationIntakeGrpc.getNotifyMedicationTakenMethod = getNotifyMedicationTakenMethod = 
              io.grpc.MethodDescriptor.<protos.MedicationPlan.MedicationTakenRequest, protos.MedicationPlan.MedicationTakenMessage>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "medicationIntake", "notifyMedicationTaken"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protos.MedicationPlan.MedicationTakenRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protos.MedicationPlan.MedicationTakenMessage.getDefaultInstance()))
                  .setSchemaDescriptor(new medicationIntakeMethodDescriptorSupplier("notifyMedicationTaken"))
                  .build();
          }
        }
     }
     return getNotifyMedicationTakenMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static medicationIntakeStub newStub(io.grpc.Channel channel) {
    return new medicationIntakeStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static medicationIntakeBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new medicationIntakeBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static medicationIntakeFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new medicationIntakeFutureStub(channel);
  }

  /**
   */
  public static abstract class medicationIntakeImplBase implements io.grpc.BindableService {

    /**
     */
    public void getMedicationIntake(protos.MedicationPlan.MedicationIntakeRequest request,
        io.grpc.stub.StreamObserver<protos.MedicationPlan.ResponseMessage> responseObserver) {
      asyncUnimplementedUnaryCall(getGetMedicationIntakeMethod(), responseObserver);
    }

    /**
     */
    public void notifyMedicationTaken(protos.MedicationPlan.MedicationTakenRequest request,
        io.grpc.stub.StreamObserver<protos.MedicationPlan.MedicationTakenMessage> responseObserver) {
      asyncUnimplementedUnaryCall(getNotifyMedicationTakenMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetMedicationIntakeMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                protos.MedicationPlan.MedicationIntakeRequest,
                protos.MedicationPlan.ResponseMessage>(
                  this, METHODID_GET_MEDICATION_INTAKE)))
          .addMethod(
            getNotifyMedicationTakenMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                protos.MedicationPlan.MedicationTakenRequest,
                protos.MedicationPlan.MedicationTakenMessage>(
                  this, METHODID_NOTIFY_MEDICATION_TAKEN)))
          .build();
    }
  }

  /**
   */
  public static final class medicationIntakeStub extends io.grpc.stub.AbstractStub<medicationIntakeStub> {
    private medicationIntakeStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationIntakeStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationIntakeStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationIntakeStub(channel, callOptions);
    }

    /**
     */
    public void getMedicationIntake(protos.MedicationPlan.MedicationIntakeRequest request,
        io.grpc.stub.StreamObserver<protos.MedicationPlan.ResponseMessage> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetMedicationIntakeMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void notifyMedicationTaken(protos.MedicationPlan.MedicationTakenRequest request,
        io.grpc.stub.StreamObserver<protos.MedicationPlan.MedicationTakenMessage> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getNotifyMedicationTakenMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class medicationIntakeBlockingStub extends io.grpc.stub.AbstractStub<medicationIntakeBlockingStub> {
    private medicationIntakeBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationIntakeBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationIntakeBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationIntakeBlockingStub(channel, callOptions);
    }

    /**
     */
    public protos.MedicationPlan.ResponseMessage getMedicationIntake(protos.MedicationPlan.MedicationIntakeRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetMedicationIntakeMethod(), getCallOptions(), request);
    }

    /**
     */
    public protos.MedicationPlan.MedicationTakenMessage notifyMedicationTaken(protos.MedicationPlan.MedicationTakenRequest request) {
      return blockingUnaryCall(
          getChannel(), getNotifyMedicationTakenMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class medicationIntakeFutureStub extends io.grpc.stub.AbstractStub<medicationIntakeFutureStub> {
    private medicationIntakeFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationIntakeFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationIntakeFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationIntakeFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<protos.MedicationPlan.ResponseMessage> getMedicationIntake(
        protos.MedicationPlan.MedicationIntakeRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetMedicationIntakeMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<protos.MedicationPlan.MedicationTakenMessage> notifyMedicationTaken(
        protos.MedicationPlan.MedicationTakenRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getNotifyMedicationTakenMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_MEDICATION_INTAKE = 0;
  private static final int METHODID_NOTIFY_MEDICATION_TAKEN = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final medicationIntakeImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(medicationIntakeImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_MEDICATION_INTAKE:
          serviceImpl.getMedicationIntake((protos.MedicationPlan.MedicationIntakeRequest) request,
              (io.grpc.stub.StreamObserver<protos.MedicationPlan.ResponseMessage>) responseObserver);
          break;
        case METHODID_NOTIFY_MEDICATION_TAKEN:
          serviceImpl.notifyMedicationTaken((protos.MedicationPlan.MedicationTakenRequest) request,
              (io.grpc.stub.StreamObserver<protos.MedicationPlan.MedicationTakenMessage>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class medicationIntakeBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    medicationIntakeBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return protos.MedicationPlan.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("medicationIntake");
    }
  }

  private static final class medicationIntakeFileDescriptorSupplier
      extends medicationIntakeBaseDescriptorSupplier {
    medicationIntakeFileDescriptorSupplier() {}
  }

  private static final class medicationIntakeMethodDescriptorSupplier
      extends medicationIntakeBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    medicationIntakeMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (medicationIntakeGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new medicationIntakeFileDescriptorSupplier())
              .addMethod(getGetMedicationIntakeMethod())
              .addMethod(getNotifyMedicationTakenMethod())
              .build();
        }
      }
    }
    return result;
  }
}
