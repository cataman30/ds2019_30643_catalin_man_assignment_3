package protos;

import java.io.ObjectInputStream.GetField;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class MyTimer {

	int secondsPassed = 45;
	int minutesPassed = 29;
	int hoursPassed = 12;
	String secondsPassedString;
	String minutesPassedString;
	String hoursPassedString;
	String dt = "2019-11-18";  // Start date
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	Calendar c = Calendar.getInstance();
	
	Timer timer = new Timer();
	TimerTask task = new TimerTask(){
		public void run() {
			secondsPassed++;
			if(secondsPassed == 60)
			{
				secondsPassed = 0;
				minutesPassed++;
			}
			if(minutesPassed == 60)
			{
				minutesPassed = 0;
				hoursPassed++;
			}
			if(hoursPassed == 24)
			{
				hoursPassed = 0;
				c.add(Calendar.DATE, 1);
				dt = sdf.format(c.getTime());
			}
		}
	};
	
	public boolean checkPassed(String hour)
	{
		String[] tokens = hour.split(":");
		int hour1 = Integer.parseInt(tokens[0]);
		int minutes = Integer.parseInt(tokens[1]);
		
		if(hoursPassed >= hour1 && minutesPassed >= minutes)
		{
			System.out.println(hoursPassed + " " + minutesPassed+"<------------------------------------------------------");
			return true;
		}
		return false;
	}
	
	public boolean checkHour(String hour1, String hour2)
	{
		String[] tokens = hour1.split(":");
		String[] tokens2 = hour2.split(":");
		
		int hourA = Integer.parseInt(tokens[0]), hourB = Integer.parseInt(tokens2[0]), minutesA = Integer.parseInt(tokens[1]), minutesB = Integer.parseInt(tokens2[1]);
		
		if(hoursPassed >= hourA && hoursPassed <= hourB + 1)
		{
			if(hoursPassed == hourA && minutesPassed >= minutesA)
				return true;
			if(hoursPassed == hourB && minutesPassed <= minutesB)
				return true;
			if(hoursPassed > hourA && hoursPassed < hourB)
				return true;
		}
		return false;
	}
	
	public String getCurrentDate()
	{
		return dt;
	}
	
	public String getTime()
	{
		if(secondsPassed/10 == 0)
			secondsPassedString = "0" + secondsPassed;
		else 
			secondsPassedString = "" + secondsPassed;
		
		if(minutesPassed/10 == 0)
			minutesPassedString = "0" + minutesPassed;
		else 
			minutesPassedString = "" + minutesPassed;
		
		if(hoursPassed/10 == 0)
			hoursPassedString = "0" + hoursPassed;
		else
			hoursPassedString = "" + hoursPassed;
			
		return hoursPassedString + ":" + minutesPassedString + ":" + secondsPassedString;
	}
	
	public void start()
	{
		try {
			c.setTime(sdf.parse(dt));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		timer.scheduleAtFixedRate(task, 1000, 1000);
	}
}
